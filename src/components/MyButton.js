import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

class MyButton extends Component {
    constructor() {
        super();

        this.namaKu = "Spongebob";
        this.pencetAku = this.pencetAku.bind(this);
    }
    
    pencetAku() {
        alert(this.namaKu);
    }
    
    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPencet}
                style={styles.tombolKu}>
                    <Text style={styles.textKu}>
                        {this.props.labelSaya}
                    </Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    tombolKu: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#CCCCCC',
        width: 100,
        height: 50,
        borderRadius: 10,
        margin: 3,
    },
    textKu: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        backgroundColor: 'transparent',
    },
});

export default MyButton;
