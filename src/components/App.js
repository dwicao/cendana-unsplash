import React, { Component } from 'react';
import Dimensions from 'Dimensions';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  TouchableOpacity,
} from 'react-native';
import MyButton from './MyButton.js';

class App extends Component {
  constructor() {
    super();

    this.state = {
      count: 0,
      imageLink: 'https://unsplash.it/200/300?image=0',
    };

    this.nomorImage = 0;
    this.max = 1000;
    this.pencetNext = this.pencetNext.bind(this);
    this.pencetRandom = this.pencetRandom.bind(this);
    this.pencetPrev = this.pencetPrev.bind(this);
  }

  pencetPrev() {
    if (this.nomorImage === 0) {
      this.nomorImage = this.nomorImage + (this.max - 1);
    } else {
      this.nomorImage = this.nomorImage - 1;
    }

    this.fetchImage();
  }

  pencetRandom() {
    const min = 500;
    const nomorAcak = Math.floor((Math.random() * (this.max - min)) + min  );

    console.log('nomorAcak', nomorAcak);

    this.nomorImage = nomorAcak;
    this.fetchImage();
  }

  pencetNext() {
    this.nomorImage = this.nomorImage + 1;
    this.fetchImage();
  }

  fetchImage() {
    this.setState({ imageLink: `https://unsplash.it/200/300?image=${this.nomorImage}` });
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={{ uri : this.state.imageLink }}
          style={styles.gambarku}
        />
        <Text>ID gambar saya adalah: {this.nomorImage}</Text>
        <View style={styles.kumpulanTombol}>
          <MyButton
            onPencet={this.pencetPrev}
            labelSaya={"\u2190"} />
          <MyButton
            onPencet={this.pencetRandom}
            labelSaya="Random" />
          <MyButton 
            onPencet={this.pencetNext}
            labelSaya={"\u2192"} />
          </View>
      </View>
    );
  }
}

const {
    width: LEBAR_DINAMIS,
    height: TINGGI_DINAMIS
  } = Dimensions.get('window');

const LEBARKU = (Platform.OS === "ios") ? 250 : 300;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  gambarku: {
    width: LEBAR_DINAMIS,
    height: TINGGI_DINAMIS * 0.5,
  },
  kumpulanTombol: {
    flexDirection: 'row',
  },
});

export default App;